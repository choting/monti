package com.fruitwishes.monitoring.services;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.JobIntentService;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fruitwishes.monitoring.AppController;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;

import org.json.JSONObject;

public class LocationUpdaterOreo extends JobIntentService implements LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {




    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    SharedPreferences mPreferences;


    private static final int JOB_ID = 1000;
    public static void enqueueWork(Context ctx, Intent intent) {
        enqueueWork(ctx, LocationUpdaterOreo.class, JOB_ID, intent);


    }


    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        init();

    }

    private void init() {
        if (isGooglePlayServicesAvailable()) {
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(10000);
            mLocationRequest.setFastestInterval(5000);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//            mLocationRequest.setSmallestDisplacement(50.0f);  /* min dist for location change, here it is 10 meter */
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(com.google.android.gms.location.LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();

            mGoogleApiClient.connect();
            Log.e("connected", "true");
        }else{
            Log.e("connected", "false");
        }
        restartService.setAlarm(false);
        stopSelf();
    }

    private boolean isGooglePlayServicesAvailable() {
        int status = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(getApplicationContext());
        return ConnectionResult.SUCCESS == status;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.e("MSG", "onLocationChanged");
        mPreferences = getSharedPreferences("CurrentUser", MODE_PRIVATE);
        String ss = mPreferences.getString("locRequest", null);
//        Location mCurrentLocation = location;
//        String Lat = String.valueOf(mCurrentLocation.getLatitude());
//        String Long = String.valueOf(mCurrentLocation.getLongitude());
//        String Latlong = Lat + "," + Long;
//        Log.e("TESTLAT", Lat + ", " + Long);
        if(ss != null){
            if (ss.equalsIgnoreCase("1")) {
                Location mCurrentLocation = location;
                String Lat = String.valueOf(mCurrentLocation.getLatitude());
                String Long = String.valueOf(mCurrentLocation.getLongitude());
                String Latlong = Lat + "," + Long;
                Log.e("TESTLAT", Lat + ", " + Long);
                upload(Latlong);
            } else if (ss.equalsIgnoreCase("2")) {
                Log.e("TESTLAT", "login to trace loc");
            }
        }
    }

    protected void startLocationUpdates() {


        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                com.google.android.gms.location.LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient,
                        builder.build());


        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        com.google.android.gms.location.LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);

    }
    private void upload(String Latlong){
        mPreferences = getSharedPreferences("CurrentUser", MODE_PRIVATE);

        String user_id = mPreferences.getString("user_id", null);
        JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET,
                "https://o2o.ph/mobile/Delivery_API/add_coordinates.php?courier=" + user_id + "&location_coordinates="+Latlong,
                null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                VolleyLog.d("", "Response: " + response.toString());
                if (response != null) {


                }




            }
        }, new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("", "Error: " + error.getMessage());
                // pDialog.dismiss();

            }
        });

        // Adding request to volley request queue
        AppController.getInstance().addToRequestQueue(jsonReq);
    }
}
