package com.fruitwishes.monitoring.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class restartService extends BroadcastReceiver {

    public static final String CUSTOM_INTENT = "com.fruitwishes.monitoring.name.restartService";
    private static Context ctx;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(restartService.class.getSimpleName(), "Service Stops! Oooooooooooooppppssssss!!!!");
        LocationUpdaterOreo.enqueueWork(context, intent);
        this.ctx = context;
//        startService(new Intent(context, LocationServices.class));
    }

    public static void cancelAlarm() {
        AlarmManager alarm = (AlarmManager) ctx.getSystemService(Context.ALARM_SERVICE);

        /* cancel any pending alarm */
        if(alarm != null){
            alarm.cancel(getPendingIntent());
        }

    }
    public static void setAlarm(boolean force) {
        cancelAlarm();
        AlarmManager alarm = (AlarmManager) ctx.getSystemService(Context.ALARM_SERVICE);
        // EVERY X MINUTES
        long delay = (1000 * 60 * 1);
        long when = System.currentTimeMillis();
        if (!force) {
            when += delay;
        }

        /* fire the broadcast */
        if(alarm != null){
            alarm.set(AlarmManager.RTC_WAKEUP, when, getPendingIntent());
        }

    }
    private static PendingIntent getPendingIntent() {
        Context ctxs;   /* get the application context */
        Intent alarmIntent = new Intent(ctx, restartService.class);
        alarmIntent.setAction(CUSTOM_INTENT);

        return PendingIntent.getBroadcast(ctx, 0, alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT);
    }
}
