package com.fruitwishes.monitoring.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fruitwishes.monitoring.AppController;
import com.fruitwishes.monitoring.R;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.fruitwishes.monitoring.utils.TextUtils.checkifEmpty;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btn_login;
    private EditText et_username, et_password;
    private SharedPreferences mPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btn_login = (Button)findViewById(R.id.btn_login);
        et_username = (EditText)findViewById(R.id.et_username);
        et_password = (EditText)findViewById(R.id.et_password);
        mPreference = getSharedPreferences("CurrentUser", MODE_PRIVATE);
        btn_login.setOnClickListener(this);



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_login:
                Login();
                break;
        }


    }

    private void Login() {
        final String user = et_username.getText().toString();
        final String pass = et_password.getText().toString();
        if(checkifEmpty(user) && checkifEmpty(pass)){
            Toast.makeText(this, "Please Enter Username and Password!", Toast.LENGTH_SHORT).show();
        }else{
            JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.GET,
                    "https://o2o.ph/mobile/Delivery_API/log_in_rider.php?username="+user+"&password="+pass, null, new Response.Listener<JSONObject>() {


                @Override
                public void onResponse(JSONObject json) {
                    Log.e("asd", user + pass);
                    Log.e("RESPONSE", json.toString());
//                    Log.e("RESPONSE", response.getString("success"));
                    try {
                        Log.e("RESPONSE", json.getString("success"));
                        if(json.getString("success").equalsIgnoreCase("1")){
                            Log.e("RESPONSE", json.toString());
                            SharedPreferences.Editor editor = mPreference.edit();
                            editor.putString("firstname", json.getString("first_name"));
                            editor.putString("lastname", json.getString("last_name"));
                            editor.putString("user_id", json.getString("ID"));
                            editor.putString("status", "1");
                            editor.putString("locRequest", "1");
                            editor.apply();
                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            finish();
                        }else{
                            Toast.makeText(LoginActivity.this, "Username & Password did not match. Please try again!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                    VolleyLog.d("", "Error: " + error.getMessage());
                    Log.e("product",error.toString());

                }
            });

            // Adding request to volley request queue
            AppController.getInstance().addToRequestQueue(myReq);


        }
    }
}
